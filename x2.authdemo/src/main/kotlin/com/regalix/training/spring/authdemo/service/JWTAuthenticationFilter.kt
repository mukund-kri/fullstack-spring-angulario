package com.regalix.training.spring.authdemo.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.regalix.training.spring.authdemo.models.AppUser
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import io.jsonwebtoken.Jwts
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.User

import java.io.IOException
import javax.servlet.FilterChain
import java.util.*

import com.regalix.training.spring.authdemo.security.*
import io.jsonwebtoken.SignatureAlgorithm

class JWTAuthenticationFilter(
        val authManager: AuthenticationManager
) : UsernamePasswordAuthenticationFilter (){

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        try {
//
//            val username: String = request.parameterMap["username"]!![0]
//            val password: String = request.parameterMap["password"]!![0]
//
//            return authManager.authenticate(
//                    UsernamePasswordAuthenticationToken(
//                            username,
//                            password,
//                            emptyList()
//                    )
//            )

            val creds: AppUser = ObjectMapper()
                    .readValue(request.inputStream, AppUser::class.java)

            return authManager.authenticate(
                    UsernamePasswordAuthenticationToken(
                            creds.username,
                            creds.password,
                            emptyList()
                    )
            )
        } catch (e: IOException) {
            throw  RuntimeException(e)
        }
    }

    override fun successfulAuthentication(request: HttpServletRequest,
                                          response: HttpServletResponse,
                                          chain: FilterChain,
                                          authResult: Authentication) {
        val token: String = Jwts.builder()
                .setSubject((authResult.principal as User).username)
                .setExpiration(Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact()
        response.addHeader(HEADER_STRING, TOKEN_PREFIX + token)
    }

}