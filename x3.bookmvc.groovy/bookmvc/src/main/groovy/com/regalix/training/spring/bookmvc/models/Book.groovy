package com.regalix.training.spring.bookmvc.models

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id


@Entity
class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id

    @Column(nullable = false, length = 512)
    String title

    @Column(nullable = false, length = 4096)
    String description

}
