package com.regalix.training.spring.authdemo.models

import javax.persistence.*


@Entity
data class Book (
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  val id: Long,

  @Column
  val isbn: String,

  @Column
  val title: String,

  @Column
  val description: String
)

