package com.regalix.training.spring.authdemo.repository

import com.regalix.training.spring.authdemo.models.Book
import org.springframework.data.repository.CrudRepository


interface BookRepository : CrudRepository<Book, Long> { }