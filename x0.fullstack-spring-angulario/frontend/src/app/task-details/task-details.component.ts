import { Component, OnInit } from '@angular/core';
import { Task } from '../models/Task';
import { TaskService } from '../task.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit {

  private task: Task;

  constructor(
    private taskService: TaskService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    let taskId = +this.route.snapshot.paramMap.get('id');
    this.taskService.getById(taskId)
      .subscribe(_task => this.task = _task);
  }

}
