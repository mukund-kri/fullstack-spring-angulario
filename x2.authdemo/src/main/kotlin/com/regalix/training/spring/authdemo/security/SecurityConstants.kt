package com.regalix.training.spring.authdemo.security

val SIGN_UP_URL = "/users/create"
val EXPIRATION_TIME: Long = 864000000 // 10 days
val SECRET = "SecretKeyToGenJWTs"
val TOKEN_PREFIX = "Bearer "
val HEADER_STRING = "Authorization"