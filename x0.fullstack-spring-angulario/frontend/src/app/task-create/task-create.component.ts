import { Component, OnInit } from '@angular/core';
import { Task } from '../models/Task';

import { TaskService } from '../task.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css']
})
export class TaskCreateComponent implements OnInit {

  task: Task = new Task({id: 0, title: "", description: ""});

  constructor(
    private taskService: TaskService,
    private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.saveTask();
  }

  saveTask() {
    this.taskService.create(this.task)
      .subscribe((task) => { console.log(task);
                             this.router.navigate(['/']); });
  }

}
