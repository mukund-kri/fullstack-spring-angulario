package com.regalix.training.spring.bookmvc.repositories

import com.regalix.training.spring.bookmvc.models.Book
import org.springframework.data.repository.CrudRepository


interface BookRepositry extends CrudRepository<Book, Long> {}
