package com.regalix.training.spring.bookapi.repository

import com.regalix.training.spring.bookapi.models.Book
import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.web.bind.annotation.CrossOrigin



interface BookRepository : CrudRepository<Book, Long> { }