import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Task } from '../models/Task';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.css']
})
export class TaskEditComponent implements OnInit {

  private task: Task = new Task({id:0, title:'', description:''});

  constructor(
    private route: ActivatedRoute,
    private taskService: TaskService,
    private router: Router
  ) { }

  ngOnInit() {
    let id: number = +this.route.snapshot.paramMap.get('id');
    this.loadTask(id);
  }

  loadTask(taskId: number) {
    this.taskService.getById(taskId)
      .subscribe(task => this.task = task);
  }

  updateTask() {
    this.taskService.update(this.task)
      .subscribe((task) => this.router.navigate(['/']));
  }

  onSubmit() {
    this.updateTask();
  }

}
