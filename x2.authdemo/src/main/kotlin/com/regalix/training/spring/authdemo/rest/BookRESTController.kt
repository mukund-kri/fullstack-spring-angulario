package com.regalix.training.spring.authdemo.rest

import org.springframework.web.bind.annotation.*

import com.regalix.training.spring.authdemo.models.Book
import com.regalix.training.spring.authdemo.repository.BookRepository;


@RestController
@CrossOrigin(origins = ["http://localhost:4200"])
class BookRESTController (
        private val bookRepository: BookRepository
){

    @GetMapping("/book/")
    fun getBooks(): List<Book> {
        return bookRepository.findAll().toList<Book>()
    }


    @GetMapping("/book/{id}")
    fun getBook(@PathVariable id: Long): Book {
        return bookRepository.findById(id).get()
    }

    @PostMapping("/book/")
    fun createBook(@RequestBody book: Book): Book {
        return bookRepository.save(book)
    }

    @DeleteMapping("/book/{id}")
    fun deleteBook(@PathVariable id: Long) {
        return bookRepository.deleteById(id)
    }

    @PutMapping("/book/{id}")
    fun updateBook(@PathVariable id: Long, @RequestBody book: Book): Book {
        return bookRepository.save(book)
    }
}