package com.regalix.training.spring.authdemo.service

import com.regalix.training.spring.authdemo.models.AppUser
import com.regalix.training.spring.authdemo.repository.UserRepository
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service


@Service
class UserDetailsServiceImpl (
        val userRepository: UserRepository
) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {

        val user: AppUser? = userRepository.findByUsername(username)


        if (user == null) {
            throw UsernameNotFoundException(username)
        }

        return User(user.username, user.password, emptyList())
    }
}