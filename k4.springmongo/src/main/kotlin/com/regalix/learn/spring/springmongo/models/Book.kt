package com.regalix.learn.spring.springmongo.models

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document


@Document
class  Book (
    @Id
    val _id: ObjectId = ObjectId(),

    val title: String,

    val description: String
) {}
