import { Component, OnInit } from '@angular/core';
import { Task } from '../models/Task';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  private tasks: Task[] = [];

  constructor(private taskService: TaskService) { }

  ngOnInit() {
    this.getAllTasks();
  }


  deleteTask(taskId: number) {
    this.taskService.deleteTask(taskId)
      .subscribe(res => {
        this.getAllTasks();
      }, (err) => {
        console.log(err);
      });
  }



  getAllTasks() {
    this.taskService.getAll()
      .subscribe(_tasks => this.tasks = _tasks)
  }
}
