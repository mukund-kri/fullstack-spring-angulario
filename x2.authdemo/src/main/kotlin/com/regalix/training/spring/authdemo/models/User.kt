package com.regalix.training.spring.authdemo.models

import javax.persistence.*


@Entity
data class AppUser (

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long,

    @Column
    val username: String,

    @Column
    var password: String
)