package org.learn.spring.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class SampleRest {

	@RequestMapping("/")
	public String homePage() {
		return "Hello Spring Boot";
	}
}
