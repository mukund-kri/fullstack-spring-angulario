export class Task {

  id: number;
  title: string;
  description: string;

  constructor({id, title, description}:
              {id: number,
               title: string,
               description: string}) {

    this.id = id;
    this.title = title;
    this.description = description;
  }
}
