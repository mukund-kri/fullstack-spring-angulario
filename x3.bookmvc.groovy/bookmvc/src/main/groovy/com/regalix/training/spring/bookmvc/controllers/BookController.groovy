package com.regalix.training.spring.bookmvc.controllers

import com.regalix.training.spring.bookmvc.models.Book
import com.regalix.training.spring.bookmvc.repositories.BookRepositry
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping

import javax.validation.Valid


@Controller
@RequestMapping(path = '/books')
class BookController {

    @Autowired
    BookRepositry bookRepositry

    Book[] books = [];
    Book editBook = new Book()

    @GetMapping(path = '/')
    def listBooks(Model model) {
        books = bookRepositry.findAll()
        model.addAttribute('books', books)
        "books/list"
    }

    @GetMapping('/add')
    def addBookForm() {
        'books/add'
    }

    @PostMapping('/add')
    def addBook(@Valid @ModelAttribute Book book) {
        book = bookRepositry.save(book)
        return 'redirect:/books/'
    }

    @GetMapping('/delete/{id}')
    def delete(@PathVariable Long id) {
        bookRepositry.deleteById(id)
        return 'redirect:/books/'
    }

    @GetMapping('/edit-form/{id}')
    def editForm(@PathVariable Long id, Model model) {
        editBook = bookRepositry.findById(id).get()
        model.addAttribute('editBook', editBook)
        return 'books/edit'
    }

}
