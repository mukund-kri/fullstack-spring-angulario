package org.learn.spring.rest;

import java.util.List;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import javax.validation.Valid;
import org.springframework.web.bind.annotation.RequestBody;
import org.learn.spring.exception.ResourceNotFoundException;
import org.learn.spring.models.Task;
import org.learn.spring.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/task")
@CrossOrigin(origins = {"http://localhost:4200"})
public class TaskRESTController {

    @Autowired
    TaskRepository taskRepository;
	
    @GetMapping("/")
    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }

    @PostMapping("/")
    public Task createTask(@Valid  @RequestBody Task task) {
        return taskRepository.save(task);
    }

    @GetMapping("/{taskId}")
    public Task getTaskById(@PathVariable Long taskId) {
    	return taskRepository.findById(taskId)
    			.orElseThrow(() -> new ResourceNotFoundException("Task", "id", taskId));
    }
    
    @PutMapping("/{taskId}")
    public Task updateTask(@PathVariable Long taskId,
    		@Valid @RequestBody Task task) {
    	
    	Task _task = taskRepository.findById(taskId)
    			.orElseThrow(() -> new ResourceNotFoundException("Task", "id", taskId));
    	_task.setTitle(task.getTitle());
    	_task.setDescription(task.getDescription());
    	
    	Task updatedTask = taskRepository.save(_task);
    	return updatedTask;
    };

    @DeleteMapping("/{taskId}")
    public ResponseEntity<?> deleteTask(@PathVariable Long taskId) {
    	Task _task = taskRepository.findById(taskId)
    			.orElseThrow(() -> new ResourceNotFoundException("Task", "id", taskId));
    	taskRepository.delete(_task);
    	
    	return ResponseEntity.ok().build();
    }
}
