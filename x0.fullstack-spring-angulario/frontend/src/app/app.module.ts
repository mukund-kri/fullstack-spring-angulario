import { BrowserModule }         from '@angular/platform-browser';
import { NgModule }              from '@angular/core';
import { HttpClientModule }      from '@angular/common/http';
import { FormsModule }           from '@angular/forms';


import { AppComponent }          from './app.component';
import { TaskService }           from './task.service';
import { TaskListComponent }     from './task-list/task-list.component';
import { TaskListItemComponent } from './task-list-item/task-list-item.component';
import { TaskDetailsComponent }  from './task-details/task-details.component';
import { TaskCreateComponent }   from './task-create/task-create.component';
import { TaskEditComponent }     from './task-edit/task-edit.component';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    TaskListComponent,
    TaskListItemComponent,
    TaskDetailsComponent,
    TaskCreateComponent,
    TaskEditComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [
    TaskService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
