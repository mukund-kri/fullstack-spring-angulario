import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map }        from 'rxjs/operators';

import { Task  }      from './models/Task';


@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private taskApiUrlRoot: string = 'http://localhost:8080/api/task/'
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };


  public tasks: Task[] = [];

  constructor(private http: HttpClient) { }

  getAll(): Observable<Task[]> {
    return this.http
      .get<Task[]>(this.taskApiUrlRoot)
      .pipe(map(data => data));
  };

  getById(taskId: number): Observable<Task> {
    return this.http
      .get<Task>(this.taskApiUrlRoot + taskId)
  }

  create(task: Task): Observable<Task> {
    return this.http.post<Task>(this.taskApiUrlRoot, task, this.httpOptions);
  }

  update(task: Task): Observable<Task> {
    return this.http.put<Task>(this.taskApiUrlRoot + task.id,
                               task,
                               this.httpOptions);
  }

  deleteTask(taskId): Observable<string> {
    return this.http
      .delete<string>(this.taskApiUrlRoot + taskId)
  }
}
