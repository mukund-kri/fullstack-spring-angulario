package com.regalix.training.spring.authdemo.repository

import com.regalix.training.spring.authdemo.models.AppUser
import org.springframework.data.repository.CrudRepository

interface UserRepository : CrudRepository<AppUser, Long> {
    fun findByUsername(username: String): AppUser
}