package com.regalix.training.spring.bookmvc

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class BookmvcApplication {

	static void main(String[] args) {
		SpringApplication.run(BookmvcApplication, args)
	}

}

