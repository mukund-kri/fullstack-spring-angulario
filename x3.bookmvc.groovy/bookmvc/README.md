# Simple MVC project

This project demos a CRUD application with old world Web MVC (non SPA). It uses spring boot with the following
configuration ...

1. Gradle for building
2. Groovy as the primary programming language
3. Postgres as the primary data store
4. JPA
5. Thymeleaf as the templating engine
