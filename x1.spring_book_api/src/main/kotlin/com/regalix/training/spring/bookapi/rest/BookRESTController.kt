package com.regalix.training.spring.bookapi.rest

import com.regalix.training.spring.bookapi.models.Book

import com.regalix.training.spring.bookapi.repository.BookRepository;
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = ["http://localhost:4200"])
class BookRESTController (
        private val bookRepository: BookRepository
){

    @GetMapping("/book/")
    fun getBooks(): List<Book> {
        return bookRepository.findAll().toList<Book>()
    }


    @GetMapping("/book/{isbn}")
    fun getBook(@PathVariable isbn: String): Book {
        return bookRepository.findOne(isbn).get()
    }

    @PostMapping("/book/")
    fun createBook(@RequestBody book: Book): Book {
        return bookRepository.save(book)
    }

    @DeleteMapping("/book/{id}")
    fun deleteBook(@PathVariable id: Long) {
        return bookRepository.deleteById(id)
    }

    @PutMapping("/book/{id}")
    fun updateBook(@PathVariable id: Long, @RequestBody book: Book): Book {
        book.id = id
        return bookRepository.save(book)
    }
}