package com.regalix.learn.spring.springmongo.repositories

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

import com.regalix.learn.spring.springmongo.SpringMongoApplication
import com.regalix.learn.spring.springmongo.models.Book
import org.junit.AfterClass


@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(SpringMongoApplication::class),
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BookRepositoryTests {

    @Autowired
    lateinit var bookRepository: BookRepository

    @Test
    fun meTest() {
        bookRepository.save(Book(title = "name", description = "description"))
        Assert.assertNotNull(1)
    }


    co
    @AfterClass
    fun cleanup() {
        bookRepository.deleteAll()
    }

}