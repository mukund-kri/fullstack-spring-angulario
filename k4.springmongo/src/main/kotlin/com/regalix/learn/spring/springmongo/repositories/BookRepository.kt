package com.regalix.learn.spring.springmongo.repositories

import com.regalix.learn.spring.springmongo.models.Book
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository


/*
 * Spring Repository Example. A spring DDD style repo over the Mongo data
 * access layer.
 */

interface BookRepository : MongoRepository<Book, ObjectId> {}