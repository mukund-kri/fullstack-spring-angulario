package com.regalix.training.spring.bookapi.models

import javax.persistence.*


@Entity
data class Book (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = -1,

    @Column()
    var title: String = "",

    @Column()
    var description: String = "",

    @Column()
    var authors: String = ""
) {}