package com.regalix.training.spring.authdemo.rest

import com.regalix.training.spring.authdemo.models.AppUser
import com.regalix.training.spring.authdemo.repository.UserRepository
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/users")
class UserController(
        private val appUserRepository: UserRepository,
        private val bCryptPasswordEncoder: BCryptPasswordEncoder
) {

    @PostMapping("/create")
    fun createUser(@RequestBody user: AppUser) {
        user.password = bCryptPasswordEncoder.encode(user.password)
        appUserRepository.save(user)
    }
}